package matrixregistration.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class RegistrationAttemptTest {

    @Test
    void does_compare_on_ip() {
        //given
        final RegistrationAttempt sut = new RegistrationAttempt("localhost", LocalDateTime.now(), 0);

        //when
        boolean result = sut.equals(new RegistrationAttempt("localhost", LocalDateTime.now(), 1));

        //then
        assertThat(result).isTrue();
    }

    @Test
    void does_return_correct_getter() {
        //given
        final LocalDateTime now = LocalDateTime.now();
        final RegistrationAttempt sut = new RegistrationAttempt("localhost", now, 0);

        //when
        final String ip = sut.getIp();
        final int registrationCount = sut.getRegistrationCount();
        final LocalDateTime registrationDate = sut.getRegistrationDate();

        //then
        assertThat(ip).isEqualTo("localhost");
        assertThat(registrationCount).isEqualTo(0);
        assertThat(registrationDate).isEqualTo(now);
    }

    @Test
    void calculates_objHash_based_on_ip() {
        //given
        final RegistrationAttempt sut = new RegistrationAttempt("localhost", LocalDateTime.now(), 0);

        //when
        final int hash = sut.hashCode();

        //then
        assertThat(hash).isEqualTo(-1204607054);
    }
}