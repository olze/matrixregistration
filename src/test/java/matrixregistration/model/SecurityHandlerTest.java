package matrixregistration.model;

import matrixregistration.handler.SecurityHandler;
import matrixregistration.service.TimeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SecurityHandlerTest {

    private final static String localhost = "localhost";
    private final TimeService timeService = mock(TimeService.class);
    private final MatrixRegistrationConfiguration matrixRegistrationConfiguration = mock(MatrixRegistrationConfiguration.class);
    private SecurityHandler sut;

    @BeforeEach
    void setUp() {
        sut = new SecurityHandler(matrixRegistrationConfiguration, timeService);
    }

    @Test
    void can_add_registrations() {
        //given

        //when
        sut.addRegistrationAttempt(localhost);
        final boolean result = sut.tooManyRequestsTried(localhost);

        //then
        assertThat(result).isFalse();
    }

    @Test
    void returns_too_many_requests_after_3_tries() {
        //given
        when(timeService.now()).thenReturn(LocalDateTime.now());
        sut = new SecurityHandler(matrixRegistrationConfiguration, timeService);

        //when
        sut.addRegistrationAttempt(localhost);
        sut.addRegistrationAttempt(localhost);
        sut.addRegistrationAttempt(localhost);

        final boolean result = sut.tooManyRequestsTried(localhost);

        //then
        assertThat(result).isTrue();
    }

    @Test
    void class_works_on_correct_attempt() {
        //given

        //when
        sut.addRegistrationAttempt(localhost);
        sut.addRegistrationAttempt("host2");
        sut.addRegistrationAttempt("host3");
        final boolean result1 = sut.tooManyRequestsTried(localhost);
        final boolean result2 = sut.tooManyRequestsTried("host2");
        final boolean result3 = sut.tooManyRequestsTried("host3");

        //then
        assertThat(result1).isFalse();
        assertThat(result2).isFalse();
        assertThat(result3).isFalse();
    }

    @Test
    void allow_registration_if_blocked_but_last_attempt_older_than_24h() {
        //given

        //when
        sut.addRegistrationAttempt(localhost);
        sut.tooManyRequestsTried(localhost);

        //then

    }

    @Test
    void does_compare_shared_secret_ok() {
        //given
        final MatrixRegistrationConfiguration matrixRegistrationConfiguration = mock(MatrixRegistrationConfiguration.class);
        when(matrixRegistrationConfiguration.getToken()).thenReturn("sha");
        sut = new SecurityHandler(matrixRegistrationConfiguration, timeService);

        //when
        boolean result = sut.isTokenOk("sha");

        //then
        assertThat(result).isTrue();
    }

    @Test
    void does_compare_shared_secret_not_ok() {
        //given
        final MatrixRegistrationConfiguration matrixRegistrationConfiguration = mock(MatrixRegistrationConfiguration.class);
        when(matrixRegistrationConfiguration.getToken()).thenReturn("sha");
        sut = new SecurityHandler(matrixRegistrationConfiguration, timeService);

        //when
        boolean result = sut.isTokenOk("sha1");

        //then
        assertThat(result).isFalse();
    }

    @Test
    void doesNotThrowExceptionWhenRegistrationNotFoundButTooManyRequestsMethodGotCalled() {
        //given

        //when
        assertDoesNotThrow(() -> sut.tooManyRequestsTried(localhost));

        //then

    }

    @Test
    void doesReturnFalseWhenTooManyRequestsTriedIsCheckedButAddRegistrationNeverGotInvoked() {
        //given

        //when
        final boolean result = sut.tooManyRequestsTried(localhost);

        //then
        assertThat(result).isFalse();
    }
}
