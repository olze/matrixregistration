package matrixregistration.ports.out;

import matrixregistration.model.MatrixRegistrationConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

@WebMvcTest(RestExecutionHandler.class)
@ContextConfiguration(classes = {MatrixRegistrationConfiguration.class})
class RestExecutionHandlerTest {

    @Autowired
    MatrixRegistrationConfiguration matrixRegistrationConfiguration;

    private RestExecutionHandler sut;

    @BeforeEach
    void setUp() {
        sut = new RestExecutionHandler(matrixRegistrationConfiguration) {
            @Override
            protected WebClient getClient() {
                try {
                    return A.webClientForAccessToken();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    @Test
    @Disabled
    void usesBasicAuthUserAndPasswd() {
        //given

        //when
        sut.registerUser("test17", "test");

        //then

    }


}
