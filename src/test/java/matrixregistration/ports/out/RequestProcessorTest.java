package matrixregistration.ports.out;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RequestProcessorTest {

    @Test
    void does_create_a_request() {
        final RequestProcessor sut = new RequestProcessor();
        final String result = sut.getRequestForRegistration("myUser", "myPass", "myNonce", "myMac");
        assertThat(result).isEqualTo("{\n" +
                "  \"nonce\": \"myNonce\",\n" +
                "  \"username\": \"myUser\",\n" +
                "  \"password\": \"myPass\",\n" +
                "  \"admin\": false,\n" +
                "  \"mac\": \"myMac\"\n" +
                "}");
    }
}