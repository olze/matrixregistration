package matrixregistration.ports.out;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class A {

    public static WebClient webClientForAccessToken() throws IOException {
        ClientResponse nonceResponse = createAccessTokenResponse();
        return createWebClientWithResponse(nonceResponse);
    }

    private static ClientResponse createAccessTokenResponse() throws IOException {
        return ClientResponse
                .create(HttpStatus.OK)
                .header("Content-Type", "application/json")
                .body(Files.readString(Paths.get("src", "test", "resources", "userTokenResponse.json")))
                .build();
    }

    private static WebClient createWebClientWithResponse(ClientResponse clientResponse) {
        Mono<ClientResponse> mono = Mono.just(clientResponse);
        ExchangeFunction exchangeFunction = request -> mono;
        return WebClient.builder().exchangeFunction(exchangeFunction).build();
    }

}
