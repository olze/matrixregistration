package matrixregistration.ports.out;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MacServiceTest {

    private MacService sut = new MacService();

    @Test
    void does_generate_correct_mac() {
        String user = "user";
        String password = "password";
        String nonce = "nonce";
        String sharedSecret = "Test";

        String result = sut.doCalc(nonce, user, password, sharedSecret);

        assertThat(result).isEqualTo("4e55a3ad843f28ff9af2ea3872222b83d363a12e");
    }


}