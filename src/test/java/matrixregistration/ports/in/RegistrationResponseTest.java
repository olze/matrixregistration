package matrixregistration.ports.in;

import matrixregistration.service.RegistrationService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RegistrationResponseTest {
    @Test
    void toJson() {
        //given
        RegistrationResponse sut = new RegistrationResponse(RegistrationService.REGISTRATION_STATE.BLOCKED, "Dödel");

        //when
        String result = sut.toJson();

        //then
        assertThat(result).isEqualTo("{\"registrationState\":\"BLOCKED\",\"username\":\"Dödel\"}");
    }
}