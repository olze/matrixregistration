package matrixregistration.ports.in;

import matrixregistration.service.RegistrationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@SpringBootTest
@AutoConfigureMockMvc
class RegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RegistrationService registrationService;

    @Test
    void shouldReturnOkIfAnyFieldIsNullOrEmpty() throws Exception {
        //given
        final LoginForm loginForm = new LoginForm("", "", "", "");

        //when
        this.mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .param("username", loginForm.getUsername())
                .param("password", loginForm.getPassword())
                .param("passwordConfirmation", loginForm.getPasswordConfirmation())
                .param("token", loginForm.getToken()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()); //TODO: how to check the content entity for e.g. INVALID_CLIENT_IP ?

        //then
        verify(registrationService, times(1))
                .register(eq(loginForm.getUsername()), eq(loginForm.getPassword()), eq(loginForm.getPasswordConfirmation()), eq(loginForm.getToken()), anyString());
    }

    @Test
    void shouldProcessRequestIfLoginformIsValid() throws Exception {
        //given
        final LoginForm loginForm = new LoginForm("username", "password", "", "regPass");

        //when
        this.mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .param("username", loginForm.getUsername())
                .param("password", loginForm.getPassword())
                .param("passwordConfirmation", loginForm.getPasswordConfirmation())
                .param("token", loginForm.getToken()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        //then
        verify(registrationService, times(1))
                .register(eq(loginForm.getUsername()), eq(loginForm.getPassword()), eq(loginForm.getPasswordConfirmation()), eq(loginForm.getToken()), anyString());
    }

    @Test
    void does_accept_xforwarded_for_headers() throws Exception {
        //given
        final LoginForm loginForm = new LoginForm("username", "password", "", "regPass");

        //when
        this.mockMvc.perform(MockMvcRequestBuilders.post("/registration")
                .param("username", loginForm.getUsername())
                .param("password", loginForm.getPassword())
                .param("passwordConfirmation", loginForm.getPasswordConfirmation())
                .param("token", loginForm.getToken())
                .header("X-Forwarded-For", "externalIp, proxyIp"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        //then
        verify(registrationService, times(1))
                .register(eq(loginForm.getUsername()), eq(loginForm.getPassword()), eq(loginForm.getPasswordConfirmation()), eq(loginForm.getToken()), anyString());
    }
}
