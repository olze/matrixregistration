package matrixregistration.handler;

import matrixregistration.handler.States.VALIDATION_STATE;
import matrixregistration.model.MatrixRegistrationConfiguration;
import matrixregistration.service.TimeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ValidationHandler.class, SecurityHandler.class,
        TimeService.class, MatrixRegistrationConfiguration.class})
class ValidationHandlerTest {

    private static final String localhost = "127.0.0.1";
    private final SecurityHandler securityHandler = mock(SecurityHandler.class);

    @Test
    void does_validate_username() {
        //given
        final ValidationHandler sut = new ValidationHandler();

        //when
        final VALIDATION_STATE result = sut.isUserAndPassOk("'", "asdf");

        //then
        assertThat(result).isEqualTo(VALIDATION_STATE.INVALID_USERNAME);
    }

    @Test
    void does_validate_password() {
        //given
        final ValidationHandler sut = new ValidationHandler();

        //when
        final VALIDATION_STATE result = sut.isUserAndPassOk("asdf", "' ");

        //then
        assertThat(result).isEqualTo(VALIDATION_STATE.INVALID_PASSWORD);
    }


    @Test
    void returns_ok_when_data_are_ok() {
        //given
        final ValidationHandler sut = new ValidationHandler();
        //when
        final VALIDATION_STATE result = sut.isUserAndPassOk("asdf", "asdf");

        //then
        assertThat(result).isEqualTo(VALIDATION_STATE.VALID);
    }


}
