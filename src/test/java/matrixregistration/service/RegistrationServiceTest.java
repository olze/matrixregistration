package matrixregistration.service;

import matrixregistration.handler.Registerable;
import matrixregistration.handler.SecurityHandler;
import matrixregistration.handler.States.VALIDATION_STATE;
import matrixregistration.handler.ValidationHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static matrixregistration.service.RegistrationService.REGISTRATION_STATE.REGISTERED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class RegistrationServiceTest {

    private final ValidationHandler validationHandler = mock(ValidationHandler.class);
    private final Registerable executionHandler = mock(Registerable.class);
    private final SecurityHandler securityHandler = mock(SecurityHandler.class);

    private final static int NO_INVOCATION = 0;
    private final static String username = "username";
    private final static String password = "password";
    private final static String passwordVerification = "password";
    private final static String token = "registrationPw";
    private final static String clientIp = "127.0.0.1";

    private RegistrationService sut;

    @BeforeEach
    void setUp() {
        sut = new RegistrationService(validationHandler, securityHandler, executionHandler);
    }

    @Test
    void doesCallSecurityServiceAndNotExecutionOnFailure() {
        //given
        when(validationHandler.isUserAndPassOk(username, password)).thenReturn(VALIDATION_STATE.INVALID_PASSWORD);

        //when
        final RegistrationService.REGISTRATION_STATE result = sut.register(username, password, passwordVerification, token, clientIp);

        //then
        assertThat(result).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_USER_OR_PASS);
        verify(validationHandler).isUserAndPassOk(eq(username), eq(password));
        verify(securityHandler, times(NO_INVOCATION)).addRegistrationAttempt(clientIp);
        verify(executionHandler, times(NO_INVOCATION)).registerUser(username, password);
    }

    @Test
    void doesCallSecurityServiceAndExecutionServiceWhenDataOk() {
        //given
        when(validationHandler.isUserAndPassOk(username, password)).thenReturn(VALIDATION_STATE.VALID);
        when(securityHandler.isTokenOk(token)).thenReturn(true);

        //when
        final RegistrationService.REGISTRATION_STATE result = sut.register(username, password, passwordVerification, token, clientIp);

        //then
        assertThat(result).isEqualTo(REGISTERED);
        verify(validationHandler).isUserAndPassOk(eq(username), eq(password));
        verify(executionHandler).registerUser(eq(username), eq(password));
        verify(securityHandler).addRegistrationAttempt(clientIp);
    }

    @Test
    void does_call_attempts_to_verify_if_ip_is_blocked() {
        //given
        when(securityHandler.tooManyRequestsTried(clientIp)).thenReturn(true);

        //when
        final RegistrationService.REGISTRATION_STATE result = sut.register(username, password, passwordVerification, token, clientIp);

        //then
        assertThat(result).isEqualTo(RegistrationService.REGISTRATION_STATE.BLOCKED);
    }

    @Test
    void does_verify_server_token() {
        //given
        when(validationHandler.isUserAndPassOk(username, password)).thenReturn(VALIDATION_STATE.VALID);
        when(securityHandler.isTokenOk(token)).thenReturn(true);

        //when
        sut.register(username, password, passwordVerification, token, clientIp);

        //then
        verify(securityHandler).isTokenOk(token);
    }

    @Test
    void does_increment_registrationAttempts_on_ok_token() {
        //given
        when(validationHandler.isUserAndPassOk(username, password)).thenReturn(VALIDATION_STATE.VALID);
        when(securityHandler.isTokenOk(token)).thenReturn(true);

        //when
        sut.register(username, password, passwordVerification, token, clientIp);

        //then
        verify(securityHandler).addRegistrationAttempt(clientIp);
    }

    @Test
    void does_increment_registrationAttempts_on_not_ok_token() {
        //given
        when(validationHandler.isUserAndPassOk(username, password)).thenReturn(VALIDATION_STATE.VALID);
        when(securityHandler.isTokenOk(token)).thenReturn(false);

        //when
        sut.register(username, password, passwordVerification, token, clientIp);

        //then
        verify(securityHandler).addRegistrationAttempt(clientIp);
        verify(executionHandler, times(NO_INVOCATION)).registerUser(username, password);
    }

    @Test
    void returns_form_validation_error_when_username_is_empty() {
        //when
        final RegistrationService.REGISTRATION_STATE register = sut.register(null, password, passwordVerification, token, clientIp);

        //then
        assertThat(register).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_USERNAME);
    }

    @Test
    void returns_form_validation_error_when_password_is_empty() {
        //when
        final RegistrationService.REGISTRATION_STATE register = sut.register(username, null, passwordVerification, token, clientIp);

        //then
        assertThat(register).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_PASSWORD);
    }

    @Test
    void returns_form_validation_error_when_token_is_empty() {
        //when
        final RegistrationService.REGISTRATION_STATE register = sut.register(username, password, passwordVerification, null, clientIp);

        //then
        assertThat(register).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_TOKEN);
    }

    @Test
    void returns_form_validation_error_when_clientIp_is_empty() {
        //when
        final RegistrationService.REGISTRATION_STATE register = sut.register(username, password, passwordVerification, token, null);

        //then
        assertThat(register).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_CLIENTIP);
    }

    @Test
    @Disabled
    void returns_form_validation_error_when_passwordVerification_is_empty() {
        //when
        final RegistrationService.REGISTRATION_STATE register = sut.register(username, password, passwordVerification, token, clientIp);

        //then
        assertThat(register).isEqualTo(RegistrationService.REGISTRATION_STATE.INVALID_PASSWORD_VERIFICATION);
    }
}
