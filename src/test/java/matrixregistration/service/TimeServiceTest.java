package matrixregistration.service;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class TimeServiceTest {

    @Test
    void does_return_correct_time_now() {
        //given
        final TimeService sut = new TimeService();

        //when
        final LocalDateTime now = sut.now();

        //then
        assertThat(now).isNotNull();
    }
}