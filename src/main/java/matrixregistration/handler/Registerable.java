package matrixregistration.handler;

public interface Registerable {

    void registerUser(String username, String password);

}
