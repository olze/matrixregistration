package matrixregistration.handler;

import matrixregistration.model.MatrixRegistrationConfiguration;
import matrixregistration.model.RegistrationAttempt;
import matrixregistration.service.TimeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashSet;

@Component
public class SecurityHandler {

    private final HashSet<RegistrationAttempt> registrations = new HashSet<>();
    private final TimeService timeService;

    private final Logger logger = LogManager.getLogger();
    private final MatrixRegistrationConfiguration matrixRegistrationConfiguration;

    public SecurityHandler(MatrixRegistrationConfiguration matrixRegistrationConfiguration, TimeService timeService) {
        this.matrixRegistrationConfiguration = matrixRegistrationConfiguration;
        this.timeService = timeService;
    }

    public void addRegistrationAttempt(String clientIP) {
        if (alreadyTriedRegistering(clientIP)) {
            synchronized (this) {
                for (final RegistrationAttempt registrationAttempt : registrations) {
                    if (registrationAttempt.getIp().equals(clientIP)) {
                        final int newRegCount = registrationAttempt.getRegistrationCount() + 1;
                        final RegistrationAttempt newRegAttempt = new RegistrationAttempt(clientIP, timeService.now(), newRegCount);
                        //this is a bit wierd. we have no replace() for sets, so i have to first remove the obj then add it
                        registrations.remove(newRegAttempt);
                        registrations.add(newRegAttempt);
                    }
                }
            }
        } else {
            registrations.add(new RegistrationAttempt(clientIP, timeService.now(), 1));
        }
    }

    private boolean alreadyTriedRegistering(String clientIp) {
        return registrations.contains(new RegistrationAttempt(clientIp));
    }

    public boolean tooManyRequestsTried(String clientIP) {

        for (final RegistrationAttempt registrationAttempt : registrations) {

            if (!registrationAttempt.equals(new RegistrationAttempt(clientIP))) {
                continue;
            }

            if (maxRegTriesReached(registrationAttempt)) {
                return !lastRegOlderThan24h(registrationAttempt);
            } else {
                return false;
            }
        }
        return false;
    }

    private boolean lastRegOlderThan24h(RegistrationAttempt registrationAttempt) {
        return registrationAttempt.getRegistrationDate().isBefore(timeService.now().minus(Duration.ofHours(24)));
    }

    private boolean maxRegTriesReached(RegistrationAttempt registrationAttempt) {
        return registrationAttempt.getRegistrationCount() >= 3;
    }

    public boolean isTokenOk(String token) {
        if (registrationTokenMatches(token)) {
            return true;
        }
        logger.debug(() -> "The shared secret from user input did not match the one from the config");
        return false;
    }

    private boolean registrationTokenMatches(String userToken) {
        final String serverToken = matrixRegistrationConfiguration.getToken();
        return serverToken.matches(userToken);
    }

}
