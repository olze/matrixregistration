package matrixregistration.handler;

import matrixregistration.handler.States.VALIDATION_STATE;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class ValidationHandler {

    private final static String USERNAME_VALIDATION_REGEX = "[a-zA-Z0-9]+";
    private final Pattern usernamePattern = Pattern.compile(USERNAME_VALIDATION_REGEX);
    private final Logger logger = LogManager.getLogger();


    public VALIDATION_STATE isUserAndPassOk(String username, String password) {

        if (notMatchesUsernameRegex(username)) {
            logger.debug(() -> username + " did not match the regex!");
            return VALIDATION_STATE.INVALID_USERNAME;
        }

        if (notMatchesPasswordCriteria(password)) {
            logger.debug(() -> password + " did not match the regex!");
            return VALIDATION_STATE.INVALID_PASSWORD;
        }

        logger.debug(() -> username + " is allowed to register");
        return VALIDATION_STATE.VALID;
    }


    private boolean notMatchesUsernameRegex(String toCheck) {
        return !usernamePattern.matcher(toCheck).matches();
    }

    private boolean notMatchesPasswordCriteria(String toCheck) {
        return toCheck.length() < 3 || toCheck.contains(" ");
    }
}
