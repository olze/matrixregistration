package matrixregistration.service;

import matrixregistration.handler.Registerable;
import matrixregistration.handler.SecurityHandler;
import matrixregistration.handler.States.VALIDATION_STATE;
import matrixregistration.handler.ValidationHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private final ValidationHandler validationHandler;
    private final SecurityHandler securityHandler;
    private final Registerable executionHandler;
    private final Logger logger = LogManager.getLogger();

    public RegistrationService(ValidationHandler validationHandler, SecurityHandler securityHandler, Registerable executionHandler) {
        this.validationHandler = validationHandler;
        this.executionHandler = executionHandler;
        this.securityHandler = securityHandler;
    }

    public REGISTRATION_STATE register(String username, String password, String passwordVerification, String token, String clientIp) {

        final REGISTRATION_STATE registration_state = validateFormFields(username, password, passwordVerification, token, clientIp);
        if (registration_state != REGISTRATION_STATE.FORM_OK) {
            return registration_state;
        }

        if (securityHandler.tooManyRequestsTried(clientIp)) {
            return REGISTRATION_STATE.BLOCKED;
        }

        final VALIDATION_STATE userAndPassOk = validationHandler.isUserAndPassOk(username, password);
        if (userAndPassOk != VALIDATION_STATE.VALID) {
            return REGISTRATION_STATE.INVALID_USER_OR_PASS;
        }

        final boolean tokenOk = securityHandler.isTokenOk(token);
        if (!tokenOk) {
            securityHandler.addRegistrationAttempt(clientIp);
            return REGISTRATION_STATE.INVALID_TOKEN;
        }

        logger.debug(() -> "Executing registration process for user " + username);
        executionHandler.registerUser(username, password);
        securityHandler.addRegistrationAttempt(clientIp);
        return REGISTRATION_STATE.REGISTERED;
    }

    private REGISTRATION_STATE validateFormFields(String username, String password, String passwordVerification, String serverToken, String clientIp) {
        if (username == null || username.isEmpty()) {
            return REGISTRATION_STATE.INVALID_USERNAME;
        }

        if (password == null || password.isEmpty()) {
            return REGISTRATION_STATE.INVALID_PASSWORD;
        }

        if (serverToken == null || serverToken.isEmpty()) {
            return REGISTRATION_STATE.INVALID_TOKEN;
        }

        if (clientIp == null || clientIp.isEmpty()) {
            return REGISTRATION_STATE.INVALID_CLIENTIP;
        }

        if (!password.equals(passwordVerification)) {
            return REGISTRATION_STATE.INVALID_PASSWORD_VERIFICATION;
        }

        return REGISTRATION_STATE.FORM_OK;
    }

    public enum REGISTRATION_STATE {
        REGISTERED, INVALID_USER_OR_PASS, BLOCKED, INVALID_TOKEN, INVALID_USERNAME, FORM_OK, INVALID_PASSWORD, INVALID_CLIENTIP, INVALID_PASSWORD_VERIFICATION
    }
}
