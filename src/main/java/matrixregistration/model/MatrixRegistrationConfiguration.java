package matrixregistration.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;

@Configuration
@PropertySource("classpath:application.properties")
@Validated
public class MatrixRegistrationConfiguration {

    @Value("${matrix.token}")
    private String token;
    @Value("${matrix.server}")
    private String server;
    @Value("${matrix.sharedSecret}")
    private String sharedSecret;


    public String getToken() {
        return token;
    }

    public String getServer() {
        return server;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

}
