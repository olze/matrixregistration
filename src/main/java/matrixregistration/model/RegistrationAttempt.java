package matrixregistration.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class RegistrationAttempt {

    private final String ip;
    private final LocalDateTime registrationDate;
    private int registrationCount;

    public RegistrationAttempt(String ip) {
        this(ip, LocalDateTime.now());
    }

    public RegistrationAttempt(String ip, LocalDateTime registrationDate) {
        this(ip, registrationDate, 0);
    }

    public RegistrationAttempt(String ip, LocalDateTime registrationDate, int registrationCount) {
        this.ip = ip;
        this.registrationDate = registrationDate;
        this.registrationCount = registrationCount;
    }

    public String getIp() {
        return ip;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public int getRegistrationCount() {
        return registrationCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationAttempt that = (RegistrationAttempt) o;
        return Objects.equals(ip, that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip);
    }
}
