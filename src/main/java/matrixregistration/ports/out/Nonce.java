package matrixregistration.ports.out;

public class Nonce {

    public String nonce;

    public Nonce() {
    }

    public Nonce(String nonce) {
        this.nonce = nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
