package matrixregistration.ports.out;

import matrixregistration.handler.Registerable;
import matrixregistration.model.MatrixRegistrationConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Component
public class RestExecutionHandler implements Registerable {

    private final WebClient client;
    private final String sharedSecret;
    private final MacService macService = new MacService();
    private final Logger logger = LogManager.getLogger();
    private final RequestProcessor requestProcessor = new RequestProcessor();

    public RestExecutionHandler(MatrixRegistrationConfiguration matrixRegistrationConfiguration) {
        client = WebClient.builder()
                .baseUrl(matrixRegistrationConfiguration.getServer())
                .build();
        this.sharedSecret = matrixRegistrationConfiguration.getSharedSecret();
    }

    @Override
    public void registerUser(String username, String password) {
        final String nonce = getNonce();
        final String mac = macService.doCalc(nonce, username, password, sharedSecret);
        final String requestBody = requestProcessor.getRequestForRegistration(username, password, nonce, mac);

        logger.debug(() -> "executing registration");
        String result = getClient().post()
                .uri(uriBuilder -> uriBuilder.pathSegment("_synapse", "admin", "v1", "register").build())
                .bodyValue(requestBody)
                .exchange()
                .flatMap(clientResponse -> {

                    if (clientResponse.statusCode() == HttpStatus.OK)
                        return clientResponse.bodyToMono(String.class);
                    else if (clientResponse.statusCode() == HttpStatus.BAD_REQUEST) {
                        return clientResponse.createException().flatMap(t -> Mono.error(new UserAlreadyExistsException()));
                    } else {
                        return clientResponse.createException().flatMap(t -> Mono.error(new RuntimeException("ERROR (token retrieval): HTTP " + t.getRawStatusCode() + " " + t.getResponseBodyAsString())));
                    }
                })
                .block();
        logger.debug(() -> "retrieved result for registration: " + result);
    }

    private String getNonce() {
        logger.debug(() -> "requesting nonce");
        final Nonce response = getClient().get()
                .uri(uriBuilder -> uriBuilder.pathSegment("_synapse", "admin", "v1", "register").build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(clientResponse -> {
                    if (clientResponse.statusCode() == HttpStatus.OK) {
                        return clientResponse.bodyToMono(Nonce.class);
                    }
                    return clientResponse.createException().flatMap(t -> Mono.error(new RuntimeException("ERROR: HTTP " + t.getRawStatusCode() + " " + t.getResponseBodyAsString())));
                })
                .block();

        if (response == null)
            throw new RuntimeException("response for user check is null!");

        return response.nonce;
    }

    //for unit test :/
    //todo: get rid of that
    protected WebClient getClient() {
        return client;
    }

}
