package matrixregistration.ports.out;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

public class MacService {

    private static final char[] HEX = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f'};

    public String doCalc(String nonce, String user, String password, String sharedSecret) {
        byte[] keyBytes = sharedSecret.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

        final Mac mac;
        try {
            mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final byte nullByte = 0;

        mac.update(nonce.getBytes(StandardCharsets.UTF_8));
        mac.update(nullByte);
        mac.update(user.getBytes(StandardCharsets.UTF_8));
        mac.update(nullByte);
        mac.update(password.getBytes(StandardCharsets.UTF_8));
        mac.update(nullByte);
        byte[] result = mac.doFinal("notadmin".getBytes(StandardCharsets.UTF_8));
        char[] hexBytes = encode(result);

        //  Covert array of Hex bytes to a String
        return new String(hexBytes);
    }

    private char[] encode(byte[] bytes) {
        final int nBytes = bytes.length;
        char[] result = new char[2 * nBytes];         //  1 hex contains two chars
        //  hex = [0-f][0-f], e.g 0f or ff

        int j = 0;
        for (byte aByte : bytes) {                    // loop byte by byte

            // 0xF0 = FFFF 0000
            result[j++] = HEX[(0xF0 & aByte) >>> 4];    // get the top 4 bits, first half hex char

            // 0x0F = 0000 FFFF
            result[j++] = HEX[(0x0F & aByte)];          // get the bottom 4 bits, second half hex char

            // combine first and second half, we get a complete hex
        }

        return result;
    }
}
