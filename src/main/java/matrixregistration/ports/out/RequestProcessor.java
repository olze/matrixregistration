package matrixregistration.ports.out;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class RequestProcessor {

    private final Logger logger = LogManager.getLogger();

    public String getRequestForRegistration(String user, String password, String nonce, String mac) {
        logger.debug(() -> "parsing user request for registration: " + user);
        final String requestBody;
        try (InputStream inputStream = getClass().getResourceAsStream("/registerUserRequest.json");
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            requestBody = reader.lines()
                    .collect(Collectors.joining(System.lineSeparator()))
                    .replace("$password", password)
                    .replace("$nonce", nonce)
                    .replace("$username", user)
                    .replace("$mac", mac);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return requestBody;
    }

}
