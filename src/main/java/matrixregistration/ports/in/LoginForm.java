package matrixregistration.ports.in;

public class LoginForm {

    private final String username;
    private final String password;
    private final String passwordConfirmation;
    private final String token;

    public LoginForm(String username, String password, String passwordConfirmation, String token) {
        this.username = username;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }
}
