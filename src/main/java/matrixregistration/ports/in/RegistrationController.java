package matrixregistration.ports.in;


import jakarta.servlet.http.HttpServletRequest;
import matrixregistration.service.RegistrationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RegistrationController {

    private final RegistrationService registrationService;
    private final HttpServletRequest request;
    private final Logger logger = LogManager.getLogger(RegistrationController.class);

    public RegistrationController(RegistrationService registrationService, HttpServletRequest request) {
        this.registrationService = registrationService;
        this.request = request;
    }

    @PostMapping("/registration")
    @ResponseBody
    public ResponseEntity<String> register(@ModelAttribute("loginForm") LoginForm loginForm) {
        logger.debug(() -> "Registration request from " + getClientIP() + " for username " + loginForm.getUsername());
        final RegistrationService.REGISTRATION_STATE regState = registrationService.register(loginForm.getUsername(),
                loginForm.getPassword(), loginForm.getPasswordConfirmation(), loginForm.getToken(), getClientIP());

        final RegistrationResponse response = new RegistrationResponse(regState, loginForm.getUsername());
        return new ResponseEntity<>(response.toJson(), HttpStatus.OK);
    }

    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

}
