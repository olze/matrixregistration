package matrixregistration.ports.in;

import matrixregistration.service.RegistrationService;

public class RegistrationResponse {

    private final RegistrationService.REGISTRATION_STATE registrationState;
    private final String username;

    public RegistrationResponse(RegistrationService.REGISTRATION_STATE registrationState, String username) {
        this.registrationState = registrationState;
        this.username = username;
    }

    public String toJson() {
        return "{\"registrationState\":\"" + registrationState + "\",\"username\":\"" + username + "\"}";
    }
}
