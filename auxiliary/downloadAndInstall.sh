#!/bin/sh

MATRIX_HOME=/home/matrix

cd $MATRIX_HOME || exit 1

CURRENT_USER=$(whoami)
if [ "$CURRENT_USER" != "matrix"  ]; then echo "Error. Not matrix user!"; exit 1; fi

wget https://gitlab.com/olze/matrixregistration/-/jobs/artifacts/master/download?job=production -O matrixregistration.zip || exit 1

CURRENT_MATRIXREG_PID=$(ps aux | grep matrixregistration | grep java | awk {'print$2'})
kill "$CURRENT_MATRIXREG_PID"

unzip -o matrixregistration.zip || exit 1

$MATRIX_HOME/matrixregistration/bin/matrixregistration &
