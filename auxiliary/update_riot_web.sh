LATEST_RELEASE=`basename $(curl -Ls -o /dev/null -w %{url_effective} https://github.com/vector-im/riot-web/releases/latest)` || exit 1

echo "LATEST RELEASE IS:  $LATEST_RELEASE"

INSTALLED_VERSION=
if [ -f "/usr/share/riot-web/VERSION.txt" ]; then
   INSTALLED_VERSION=`cat /usr/share/riot-web/VERSION.txt`
fi

if [ $INSTALLED_VERSION = $LATEST_RELEASE ]; then
   echo "up 2 date with $INSTALLED_VERSION"
   exit 0
fi

curl -L https://github.com/vector-im/riot-web/releases/download/$LATEST_RELEASE/riot-$LATEST_RELEASE.tar.gz --output /tmp/riot-web.tar.gz || exit 1

cd /tmp || exit 1
tar xzf riot-web.tar.gz || exit 1
mv riot-$LATEST_RELEASE riot-web || exit 1

cp /usr/share/riot-web/config.riot.trustserv.de.json /tmp/riot-web/ || exit 1
rm -rf /usr/share/riot-web || exit 1
mv /tmp/riot-web /usr/share || exit 1
echo $LATEST_RELEASE > /usr/share/riot-web/VERSION.txt

echo "Updated to $LATEST_RELEASE"
