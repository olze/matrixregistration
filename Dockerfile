FROM amd64/adoptopenjdk:17-jre-hotspot
RUN mkdir /opt/app
ADD build/distributions/matrixregistration-boot.tar /opt
CMD ["/opt/matrixregistration-boot/bin/matrixregistration", "start"]