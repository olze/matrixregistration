# MatrixRegistration

A registration platform to allow people with a shared secret to register matrix accounts on your homeserver. Even when 
the matrix server usually does NOT allow registrations. A simple protection against abuse is included, IPs are blocked after 
too many registration attempts.

![alt text](/auxiliary/matreg.png)

## Getting Started

If you want to contribute or check the sources, just clone them with
```
git clone https://gitlab.com/olze/matrixregistration.git
```
### Prerequisites

What things you need to install the software and how to install them

```
Java Runtime Environment, version 11+
```

### Installing

Download the latest version, see "releases" from gitlab or take the following command to download the latest release

```
wget https://gitlab.com/olze/matrixregistration/-/jobs/artifacts/master/download?job=production -O matrixregistration.zip || exit 1
```

Extract the archive

```
unzip matrixregistration.zip
```

Modify the application.properties file to your needs, e.g.:

```
matrix.token=<THE REGISTRATION REQUIRED PASSWORD>
matrix.server=https://my.server.com
matrix.sharedSecret=<THE SAME AS IN YOUR HOMESERVER.YAML>
```

The **matrix.token** is the token the user has to enter when he wants to register. This has nothing to do with the shared secret
in the homeserver.yml!

The **matrix.adminUser** is the user that is used when registering a new user. This user must have admin permissions in your
homeserver. Therefore i would chmod 600 the application.properties file, as it contains this password in plain text!

The **matrix.adminPassword** is the password for the matrix.adminUser

The **matrix.server** is the location of your homeserver. 

Run the application, the working directory should be the folder "matrixregistration". Otherwise make sure to have your 
application.properties in ".".

```
./bin/matrixregistration start
```

Now you can connect to http://localhost:8080 or, what i would recommend, use nginx/apache2 as reverse proxy and redirect 
the requests. Further information (podman/docker, configuration for apache2/nginx etc.) will follow.

## Logging

If you'd like to override the default logging level (INFO), pass -Dlogging.config=/home/yourUser/log4j2.xml to the jvm params and configure log4j2.xml as needed.

## Deployment

### systemd
If you want to add the service to your distribution, just copy the auxiliary/matrixregistration.service file to /etc/systemd/system and do: 
```
systemctl daemon-reload
systemctl enable matrixregistration
systemctl start matrixregistration
``` 
Make sure to modify that file for your needs!

### Docker/podman

Using podman/docker is straight forward.
At the moment, there is no image registry (wip) so you need to build the image yourself!

Download the repository and execute the following commands in it.

First, build the image

```
  oli @ ~/matrixregistration - [master] $ podman build .
  STEP 1: FROM amd64/adoptopenjdk:11-jre-hotspot
  STEP 2: RUN mkdir /opt/app
  --> Using cache 4dca73cfc0ef6a845e9d5cfbe23bc9f03f80cd68eafbdf3b0100f8e23d9fbc69
  STEP 3: ADD build/distributions/matrixregistration-boot.tar /opt
  --> Using cache 3215976985b709d4ec8e453286939b80a1b05bb7b9b7aa7c94a92da936d9356c
  STEP 4: CMD ["/opt/matrixregistration-boot/bin/matrixregistration", "start"]
  --> Using cache 443f693a1218a652ac4fee67c732cdc4526fd1872ae1e062352eb28f02db8fe8
  443f693a1218a652ac4fee67c732cdc4526fd1872ae1e062352eb28f02db8fe8
```
Then, run it with
```
podman run -p 8080:8080/tcp 
     -e MATRIX_TOKEN=$yourToken
     -e MATRIX_SERVER=$yourServer 
     -e MATRIX_SHAREDSECRET=$sameAsInYourHomeserverYaml 
     443f693a1218a652ac4fee67c732cdc4526fd1872ae1e062352eb28f02db8fe8
```

## Built With


## Contributing


## Versioning


## Authors

* **Oliver Z.** - *Initial work* 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks to https://github.com/ZerataX/matrix-registration for its nice web ui + .js/.html!
